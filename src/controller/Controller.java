package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller
{

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values)
	{
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag)
	{
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegersBag bag)
	{
		return model.getMax(bag);
	}
	//Get the minimun in the bag
	public static double getMin(IntegersBag bag)
	{
		return model.getMin(bag);
	}
	//Get the bag's sum
	public static double getSum (IntegersBag bag)
	{
		return model.bagSum(bag);
	}
	//Gets the substraction of the bag
	public static double  getSubs(IntegersBag bag)
	{
		return model.bagSubtraction(bag);
	}
	//Gets the multiplication of the bag
	public static double getMultiplication(IntegersBag bag)
	{
		return  model.multiplicationBag(bag);
	}
}
