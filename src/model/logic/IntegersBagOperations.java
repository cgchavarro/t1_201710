package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}
	//Finds the minimun value in the actual bag
	public int getMin(IntegersBag bag)
	{
		int min = Integer.MAX_VALUE;
		int value;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if( min > value){
					min = value;
				}
			}

		}
		return min;
	}

	//Gets the total sum of bag values
	public int bagSum(IntegersBag bag )

	{
		int sum = 0;
		int actualValue;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				actualValue = iter.next();
				sum+= actualValue;
			}
		}

		return sum;
	}


	//Multuplies bag's values
	public int multiplicationBag (IntegersBag bag )
	{

		int result;
		Iterator<Integer> iter = bag.getIterator();
		if (iter.hasNext())
		{
			result = 1;
		}
		else
		{
			result = 0;
		}
		if(bag != null)
		{
			while(iter.hasNext())
			{
				result = result*iter.next();

			}
		}
		return result;
	}

	
	//Gets the total subtraction of bag values
		public int bagSubtraction(IntegersBag bag )

		{
			int subs = 0;
		//	int actualValue;
			Iterator<Integer> iter = bag.getIterator();
			if(iter.hasNext())
			{
				subs=iter.next();
			}
			
				while(iter.hasNext())
				{
					
					subs-=  iter.next();
				}
			
			return subs;
		}
	
//	public int bagSubstraction(IntegersBag bag)
//	{
//		int subs = 0;
//		int value;
//		Iterator<Integer> iter = bag.getIterator();
//		boolean hasNext = iter.hasNext();
//		if(hasNext)
//		{
//			subs = iter.next();
//			while(hasNext)
//			{
//				value = iter.next().;
//				subs-=value;
//			}
//		}
//		
//		return subs;
//	}
}
